from django.test import TestCase
from django.contrib.auth import get_user_model
from .models import Post


class BlogTestes(TestCase):
    def setUp(self) -> None:
        self.user = get_user_model().objects.create_user(username='0000',
                                                        email='',
                                                        password='')

        self.post = Post.objects.create(title='A good title',
                                        body='Not bad body content',
                                        author=self.user)

        self.client.login(username='0000', password='')

    def test_post_content(self):
        self.assertEqual(str(self.post.title), 'A good title')
        self.assertEqual(str(self.post.body), 'Not bad body content')

    def test_post_list_view(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'Not bad body content')
        self.assertTemplateUsed(resp, 'home.html')

    def test_post_details(self):
        resp = self.client.get('/post/1/')
        no_resp = self.client.get('/post/20/')

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(no_resp.status_code, 404)
        self.assertTemplateUsed(resp, 'post_detail.html')

    def test_post_create(self):

        form_values = {
            'title': 'new title',
            'author': self.user.pk,
            'body': 'new body',
        }

        resp = self.client.post('/post/new/', data=form_values)
        self.assertEqual(resp.status_code, 302)

        last = Post.objects.last()
        self.assertEqual(last.title, form_values['title'])
        self.assertEqual(last.body, form_values['body'])

    def test_post_update_view(self):

        form_values = {
            'title': 'updated title',
            'author': self.user.pk,
            'body': 'updated body',
        }

        resp = self.client.post(f'/post/{self.post.id}/edit/', data=form_values)
        self.assertEqual(resp.status_code, 302)

        post = Post.objects.get(id=self.user.pk)
        self.assertEqual(post.title, form_values['title'])
        self.assertEqual(post.body, form_values['body'])

    def test_post_delete_view(self):
        count = Post.objects.count()

        resp = self.client.post(f'/post/{self.post.id}/delete/')

        self.assertEqual(resp.url, f'/')
        self.assertEqual(resp.status_code, 302)

        new_count = Post.objects.count()
        self.assertEqual(count - 1, new_count)

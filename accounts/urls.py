from django.urls import path

from . import views as views_my


urlpatterns = [
    path('signup/', views_my.SignUPView.as_view(), name='signup'),
]
